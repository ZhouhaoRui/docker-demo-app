# install node
FROM node:13-alpine

# set MONGO_DB_USERNAME=admin MONGO_DB_PWD=password
ENV MONGO_DB_USERNAME=admin \
    MONGO_DB_PWD=password

# create home/app folder
RUN mkdir -p /home/app

# copy current folder files to /home/app
COPY ./app /home/app

WORKDIR /home/app

RUN npm install

# start node server with node command
CMD [ "node", "server.js" ]